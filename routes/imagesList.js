/**
 * Created by mohan on 1/31/17.
 */
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    const testFolder = '../media/';
    const fs = require('fs');
    var imageList=[];
    fs.readdir(testFolder, function (err, items) {
        console.log(items);
        res.send(items);
    });
});

module.exports = router;
